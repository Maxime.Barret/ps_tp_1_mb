#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>

int main()
{
  printf("HELLO !\n");
  while(1)
  {
    printf("ID = %d\n", getpid());
    printf("ID pere = %d\n", getppid());
    printf("Nombre alea = %d\n", rand() % 100);
    sleep(1);
  }
  printf("EXIT\n");
  return EXIT_SUCCESS; 
}