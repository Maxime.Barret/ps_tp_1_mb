#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdbool.h>
#include <signal.h>

bool running;
void stop_handler(int sig);
void exit_message();

int main()
{
  running = true;
  pid_t cpid;   // type pid pour contenir les pid des process
  int wstatus;  // statut du wait, modifié dans la fonction waitpid

  // Structure de gestion de l'arret du processus
  struct sigaction new_action;
  new_action.sa_handler = stop_handler;

  printf("HELLO !\n");

  // Installation des prises en compte de signaux
  sigaction(SIGINT, &new_action, NULL);
  sigaction(SIGTERM, &new_action, NULL);
  atexit(&exit_message);

  // Fork du processus
  cpid = fork();
  if(cpid == -1)
  {
    printf("ERR -- Fork-- \n");
    exit(EXIT_FAILURE);
  }

  // Boucle principale
  while(running && !WIFSTOPPED(wstatus))
  {
    cpid = waitpid(cpid, &wstatus, WUNTRACED);  // waitpid pour prendre en compte le pid du processus enfant

    printf("ID = %d\n", getpid());
    printf("ID pere = %d\n", getppid());
    printf("Nombre alea = %d\n", rand() % 100);
    sleep(1);
  }
  printf("EXIT\n");
  return EXIT_SUCCESS; 
}

// @brief : affiche le numero de signal reçu et arrete la boucle principale
void stop_handler(int sig)
{
  printf("\nSIGNAL : %d\n", sig);
  running = false;
}

// @brief : afiche un message a la sortie du processus
void exit_message()
{
  printf("-- EXIT --\n");
}