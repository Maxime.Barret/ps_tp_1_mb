#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdbool.h>
#include <signal.h>

bool running;
void stop_handler(int sig);
void exit_message();

int main()
{
  running = true; 
  struct sigaction new_action;
  new_action.sa_handler = stop_handler;

  printf("HELLO !\n");
  sigaction(SIGINT, &new_action, NULL);
  sigaction(SIGTERM, &new_action, NULL);
  atexit(&exit_message);
  while(running)
  {
    printf("ID = %d\n", getpid());
    printf("ID pere = %d\n", getppid());
    printf("Nombre alea = %d\n", rand() % 100);
    sleep(1);
  }
  printf("EXIT\n");
  return EXIT_SUCCESS; 
}

void stop_handler(int sig)
{
  printf("\nSIGNAL : %d\n", sig);
  running = false;
}

void exit_message()
{
  printf("-- EXIT --\n");
}